USE [ApplicationTrackingSystem]
GO
/****** Object:  Table [dbo].[candidate]    Script Date: 09-11-2020 12:33:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[candidate](
	[candidate_id] [int] IDENTITY(1,1) NOT NULL,
	[first_name] [varchar](20) NOT NULL,
	[last_name] [varchar](20) NULL,
	[middle_name] [varchar](20) NULL,
	[date_of_birth] [datetime] NOT NULL,
	[total_years_of_experience] [float] NOT NULL,
	[employment_status] [int] NOT NULL,
	[current_employer] [varchar](50) NULL,
	[email] [varchar](50) NULL,
 CONSTRAINT [PK_candidate] PRIMARY KEY CLUSTERED 
(
	[candidate_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[expertise_level_lookup]    Script Date: 09-11-2020 12:33:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[expertise_level_lookup](
	[expertise_level_id] [int] IDENTITY(1,1) NOT NULL,
	[expertise_level] [varchar](50) NULL,
	[score] [int] NULL,
 CONSTRAINT [PK_expertise_level_lookup] PRIMARY KEY CLUSTERED 
(
	[expertise_level_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[job_application]    Script Date: 09-11-2020 12:33:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[job_application](
	[application_id] [int] IDENTITY(1,1) NOT NULL,
	[job_id] [int] NULL,
	[candidate_id] [int] NULL,
	[resume] [varchar](250) NULL,
	[rating] [float] NULL,
 CONSTRAINT [PK_job_application] PRIMARY KEY CLUSTERED 
(
	[application_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[job_description]    Script Date: 09-11-2020 12:33:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[job_description](
	[job_id] [int] IDENTITY(1,1) NOT NULL,
	[job_title] [varchar](50) NOT NULL,
	[job_code] [varchar](10) NULL,
	[job_description] [varchar](250) NOT NULL,
	[created_date] [datetime] NOT NULL,
	[expiry_date] [datetime] NULL,
	[status] [varchar](20) NULL,
	[contact_email] [varchar](50) NULL,
 CONSTRAINT [PK_job_description] PRIMARY KEY CLUSTERED 
(
	[job_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[job_skill_weightage_applied]    Script Date: 09-11-2020 12:33:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[job_skill_weightage_applied](
	[job_skill_weightage_applied_id] [int] IDENTITY(1,1) NOT NULL,
	[application_id] [int] NULL,
	[skill_id] [int] NULL,
	[expertise_level_id] [int] NULL,
 CONSTRAINT [PK_job_skill_weightage_applied] PRIMARY KEY CLUSTERED 
(
	[job_skill_weightage_applied_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[job_skill_weightage_required]    Script Date: 09-11-2020 12:33:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[job_skill_weightage_required](
	[job_skill_weightage_required_id] [int] IDENTITY(1,1) NOT NULL,
	[job_id] [int] NOT NULL,
	[skill_id] [int] NOT NULL,
	[expertise_level_id] [int] NULL,
	[minimum_years_of_experience_required] [float] NULL,
	[maximum_years_of_experience_required] [float] NULL,
	[is_mandatory] [char](1) NULL,
	[weightage] [float] NULL,
 CONSTRAINT [PK_job_skill_weightage_required] PRIMARY KEY CLUSTERED 
(
	[job_skill_weightage_required_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[skill_set]    Script Date: 09-11-2020 12:33:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[skill_set](
	[skill_id] [int] IDENTITY(1,1) NOT NULL,
	[description] [varchar](50) NOT NULL,
	[is_enabled] [int] NULL,
 CONSTRAINT [PK_skill_set] PRIMARY KEY CLUSTERED 
(
	[skill_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[candidate] ON 

INSERT [dbo].[candidate] ([candidate_id], [first_name], [last_name], [middle_name], [date_of_birth], [total_years_of_experience], [employment_status], [current_employer], [email]) VALUES (1, N'John', N'David', N'J', CAST(N'1990-12-26T00:00:00.000' AS DateTime), 5, 1, N'ABC', N'john@gmail.com')
SET IDENTITY_INSERT [dbo].[candidate] OFF
SET IDENTITY_INSERT [dbo].[expertise_level_lookup] ON 

INSERT [dbo].[expertise_level_lookup] ([expertise_level_id], [expertise_level], [score]) VALUES (1, N'Beginner', 4)
INSERT [dbo].[expertise_level_lookup] ([expertise_level_id], [expertise_level], [score]) VALUES (2, N'Intermediate', 6)
INSERT [dbo].[expertise_level_lookup] ([expertise_level_id], [expertise_level], [score]) VALUES (3, N'Advanced', 10)
SET IDENTITY_INSERT [dbo].[expertise_level_lookup] OFF
SET IDENTITY_INSERT [dbo].[job_application] ON 

INSERT [dbo].[job_application] ([application_id], [job_id], [candidate_id], [resume], [rating]) VALUES (2, 2, 1, N'jlkj/sdf', 7.1)
SET IDENTITY_INSERT [dbo].[job_application] OFF
SET IDENTITY_INSERT [dbo].[job_description] ON 

INSERT [dbo].[job_description] ([job_id], [job_title], [job_code], [job_description], [created_date], [expiry_date], [status], [contact_email]) VALUES (2, N'QA Analyst', N'QAA', N'Should be an expert in Manual and Automation Testing.', CAST(N'2020-08-16T00:00:00.000' AS DateTime), CAST(N'2020-10-16T00:00:00.000' AS DateTime), N'published', NULL)
INSERT [dbo].[job_description] ([job_id], [job_title], [job_code], [job_description], [created_date], [expiry_date], [status], [contact_email]) VALUES (3, N'DB Administrator', N'DBA', N'Should have strong knowledge in SQL.', CAST(N'2020-08-01T00:00:00.000' AS DateTime), CAST(N'2020-10-16T00:00:00.000' AS DateTime), N'published', NULL)
SET IDENTITY_INSERT [dbo].[job_description] OFF
SET IDENTITY_INSERT [dbo].[job_skill_weightage_applied] ON 

INSERT [dbo].[job_skill_weightage_applied] ([job_skill_weightage_applied_id], [application_id], [skill_id], [expertise_level_id]) VALUES (1, 2, 1, 1)
INSERT [dbo].[job_skill_weightage_applied] ([job_skill_weightage_applied_id], [application_id], [skill_id], [expertise_level_id]) VALUES (2, 2, 2, 2)
INSERT [dbo].[job_skill_weightage_applied] ([job_skill_weightage_applied_id], [application_id], [skill_id], [expertise_level_id]) VALUES (3, 2, 3, 2)
SET IDENTITY_INSERT [dbo].[job_skill_weightage_applied] OFF
SET IDENTITY_INSERT [dbo].[job_skill_weightage_required] ON 

INSERT [dbo].[job_skill_weightage_required] ([job_skill_weightage_required_id], [job_id], [skill_id], [expertise_level_id], [minimum_years_of_experience_required], [maximum_years_of_experience_required], [is_mandatory], [weightage]) VALUES (1, 2, 1, 1, 1, 4, N'y', 1)
INSERT [dbo].[job_skill_weightage_required] ([job_skill_weightage_required_id], [job_id], [skill_id], [expertise_level_id], [minimum_years_of_experience_required], [maximum_years_of_experience_required], [is_mandatory], [weightage]) VALUES (2, 2, 2, 2, 1, 2, N'y', 1)
INSERT [dbo].[job_skill_weightage_required] ([job_skill_weightage_required_id], [job_id], [skill_id], [expertise_level_id], [minimum_years_of_experience_required], [maximum_years_of_experience_required], [is_mandatory], [weightage]) VALUES (3, 2, 3, 1, 1, 2, N'n', 0.25)
SET IDENTITY_INSERT [dbo].[job_skill_weightage_required] OFF
SET IDENTITY_INSERT [dbo].[skill_set] ON 

INSERT [dbo].[skill_set] ([skill_id], [description], [is_enabled]) VALUES (1, N'html', 1)
INSERT [dbo].[skill_set] ([skill_id], [description], [is_enabled]) VALUES (2, N'jquery', 1)
INSERT [dbo].[skill_set] ([skill_id], [description], [is_enabled]) VALUES (3, N'selenium', 1)
INSERT [dbo].[skill_set] ([skill_id], [description], [is_enabled]) VALUES (4, N'javascript', 1)
INSERT [dbo].[skill_set] ([skill_id], [description], [is_enabled]) VALUES (5, N'angular', 1)
INSERT [dbo].[skill_set] ([skill_id], [description], [is_enabled]) VALUES (6, N'MS SQL', 1)
INSERT [dbo].[skill_set] ([skill_id], [description], [is_enabled]) VALUES (7, N'MySql', 1)
INSERT [dbo].[skill_set] ([skill_id], [description], [is_enabled]) VALUES (8, N'Oracle', 1)
INSERT [dbo].[skill_set] ([skill_id], [description], [is_enabled]) VALUES (9, N'MongoDB', NULL)
INSERT [dbo].[skill_set] ([skill_id], [description], [is_enabled]) VALUES (10, N'CouchDB', NULL)
SET IDENTITY_INSERT [dbo].[skill_set] OFF
ALTER TABLE [dbo].[job_application]  WITH CHECK ADD  CONSTRAINT [FK_job_application_candidate] FOREIGN KEY([candidate_id])
REFERENCES [dbo].[candidate] ([candidate_id])
GO
ALTER TABLE [dbo].[job_application] CHECK CONSTRAINT [FK_job_application_candidate]
GO
ALTER TABLE [dbo].[job_application]  WITH CHECK ADD  CONSTRAINT [FK_job_application_job_description] FOREIGN KEY([job_id])
REFERENCES [dbo].[job_description] ([job_id])
GO
ALTER TABLE [dbo].[job_application] CHECK CONSTRAINT [FK_job_application_job_description]
GO
ALTER TABLE [dbo].[job_skill_weightage_applied]  WITH CHECK ADD  CONSTRAINT [FK_job_skill_weightage_applied_expertise_level_lookup] FOREIGN KEY([expertise_level_id])
REFERENCES [dbo].[expertise_level_lookup] ([expertise_level_id])
GO
ALTER TABLE [dbo].[job_skill_weightage_applied] CHECK CONSTRAINT [FK_job_skill_weightage_applied_expertise_level_lookup]
GO
ALTER TABLE [dbo].[job_skill_weightage_applied]  WITH CHECK ADD  CONSTRAINT [FK_job_skill_weightage_applied_job_application] FOREIGN KEY([application_id])
REFERENCES [dbo].[job_application] ([application_id])
GO
ALTER TABLE [dbo].[job_skill_weightage_applied] CHECK CONSTRAINT [FK_job_skill_weightage_applied_job_application]
GO
ALTER TABLE [dbo].[job_skill_weightage_applied]  WITH CHECK ADD  CONSTRAINT [FK_job_skill_weightage_applied_skill_set] FOREIGN KEY([skill_id])
REFERENCES [dbo].[skill_set] ([skill_id])
GO
ALTER TABLE [dbo].[job_skill_weightage_applied] CHECK CONSTRAINT [FK_job_skill_weightage_applied_skill_set]
GO
ALTER TABLE [dbo].[job_skill_weightage_required]  WITH CHECK ADD  CONSTRAINT [FK_job_skill_weightage_required_expertise_level_lookup] FOREIGN KEY([expertise_level_id])
REFERENCES [dbo].[expertise_level_lookup] ([expertise_level_id])
GO
ALTER TABLE [dbo].[job_skill_weightage_required] CHECK CONSTRAINT [FK_job_skill_weightage_required_expertise_level_lookup]
GO
ALTER TABLE [dbo].[job_skill_weightage_required]  WITH CHECK ADD  CONSTRAINT [FK_job_skill_weightage_required_job_description] FOREIGN KEY([job_id])
REFERENCES [dbo].[job_description] ([job_id])
GO
ALTER TABLE [dbo].[job_skill_weightage_required] CHECK CONSTRAINT [FK_job_skill_weightage_required_job_description]
GO
ALTER TABLE [dbo].[job_skill_weightage_required]  WITH CHECK ADD  CONSTRAINT [FK_job_skill_weightage_required_skill_set] FOREIGN KEY([skill_id])
REFERENCES [dbo].[skill_set] ([skill_id])
GO
ALTER TABLE [dbo].[job_skill_weightage_required] CHECK CONSTRAINT [FK_job_skill_weightage_required_skill_set]
GO
